<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Resume;
use AppBundle\Form\ResumeType;

/**
 * @Route("/resume")
 */
class ResumeController extends Controller
{
    /**
     * @Route("/list/user", name="resume_list_user")
     */
    public function listPerUserAction(Request $request)
    {
        $user = $this->getUser();

        if (!$user) {
            return $this->redirect('AppBundle:Resume:listAll');
        }

        $cvs = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Resume')
            ->getByUser($user);

        // replace this example code with whatever you need
        return $this->render('resume/list.html.twig', [
            'cvs' => $cvs
        ]);
    }

    /**
     * @Route("/list/all", name="resume_list_all")
     */
    public function listAllAction(Request $request)
    {
        $cvs = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Resume')
            ->findAll();

        // replace this example code with whatever you need
        return $this->render('resume/list.html.twig', [
            'cvs' => $cvs
        ]);
    }

    /**
     * @Route("/add", name="resume_add_form")
     */
    public function addFormAction(Request $request)
    {
        $resume = new Resume();
        $form = $this->createForm(ResumeType::class, $resume);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $file stores the uploaded PDF file
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $resume->getFile();

            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            // Move the file to the directory where brochures are stored
            $file->move(
                $this->getParameter('resume_directory'),
                $fileName
            );

            // Update the 'brochure' property to store the PDF file name
            // instead of its contents
            $resume->setFile($fileName);

            // ... persist the $product variable or any other work
            $resume->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($resume);
            $em->flush();

            return $this->redirect($this->generateUrl('resume_list_all'));
        }


        // replace this example code with whatever you need
        return $this->render('resume/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

}