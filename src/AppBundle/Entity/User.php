<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();

        $this->resume = new ArrayCollection();
    }

    /**
     * One Product has Many resumes.
     * @ORM\OneToMany(targetEntity="Resume", mappedBy="user")
     */
    private $resume;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    private $lastname;

    /**
     * Add resume
     *
     * @param \AppBundle\Entity\Resume $resume
     *
     * @return User
     */
    public function addResume(\AppBundle\Entity\Resume $resume)
    {
        $this->resume[] = $resume;

        return $this;
    }

    /**
     * Remove resume
     *
     * @param \AppBundle\Entity\Resume $resume
     */
    public function removeResume(\AppBundle\Entity\Resume $resume)
    {
        $this->resume->removeElement($resume);
    }

    /**
     * Get resume
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * Set name
     *
     * @param string $firstname
     *
     * @return Resume
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set name
     *
     * @param string $lastname
     *
     * @return Resume
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }
}
